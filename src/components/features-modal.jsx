import React, { useState } from "react";

import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";


const FeaturesModal = () => {

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => {
        window.gtag("event", "view_item", {items: [{id: "button", name: 'View All Features'}]})
        return setShow(true)
    };

    return (
        <>
        <Button variant="secondary" onClick={handleShow} className="modalButton">
            See all Features & Appointments
        </Button>
        <Modal 
            show={show} 
            onHide={handleClose} 
            size="lg"
        >
            <Modal.Header closeButton>
                <Modal.Title>42 Park Lane - Appointments</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="featuresModal">
                <h3>QUALITY EXTERIOR FEATURES</h3>
                <ul>
                    <li>Architecturally custom designed home.</li>
                    <li>Maintenance free clay brick and stone combination.</li>
                    <li>Pre-finished maintenance-free aluminum soffits, fascia, eavestrough and down pipe. Custom design, maintenance free aluminum Long Board</li>
                    <li>Self sealing high grade shingles with a 30 year manufacturers warranty.</li>
                    <li>Maintenance free, EnergyStar Rated vinyl windows, LowE argon gas.</li>
                    <li>Two(2) exterior water taps with interior shut-off valves.</li>
                    <li>Two(2) exterior waterproof electrical outlets with ground fault interrupters (GFCI).</li>
                    <li>Three car garage with large side driveway.</li>
                    <li>370 square feet of rear covered portico (loggia) with custom ordered Douglas Fir post and beam designed, railings and backyard accessibility.</li>
                    <li>Thirty five (35) exterior soffit pot lights.</li>
                    <li>2 Soffit receptacles for holiday lighting.</li>
                </ul>
                
                <h3>QUALITY INTERIOR FEATURES</h3>
                <ul>
                    <li>Soaring 10’-6” ceilings on main floor through out with 12’-10” high ceilings in the great room and breakfast area. Full 9ft ceilings on second floor and basement.</li>
                    <li>Custom designed cantilevered stairs with glass railings. </li>
                    <li>Architecturally inspired interior hardware finishings.</li>
                    <li>luxury six(6) inch white oak hardwood flooring and ceramic tile throughout main floor and second floor loft area.</li>
                    <li>Baseboard 7-1/4”, Casing 2-3/4”.</li>
                    <li>All interior walls to be painted in high quality latex paint. Kitchen, Laundry and bathrooms to be semi-gloss. All interior trim painted with semi-gloss.</li>
                    <li>Nailed and screwed 5/8” tongue and groove spruce plywood on first and second subfloors.</li>
                    <li>Main floor and second floor ceilings finished with smooth surfaces.  </li>
                    <li>Architecturally inspired recessed channels in the greatroom and breakfast area</li>
                    <li>8’ interior doors on main floor.</li>
                    <li>6’-8” interior doors on second floor.</li>
                    <li>Coffered ceiling in master bedroom.</li>
                    <li>Quartz counter top in kitchen, powder room and master ensuite.</li>
                    <li>Natural gas fireplace in great room with surround mantle and hearth.</li>
                    <li>Large basement windows.</li>
                    <li>Tarion warranty.</li>
                    <li>Three window clearstory overlooking main stairs and second floor common area.</li>
                    <li>Stair lights.</li>
                </ul>
                
                <h3>SUPERIOR CONSTRUCTION</h3>
                <ul>
                    <li>All framing construction provided by KJT Carpentry Inc., backed with over 20 years of quality workmanship and outstanding customer service.</li>
                    <li>Superior 2”X6” wood frame construction on exterior walls.</li>
                    <li>Poured concrete foundation walls with heavy duty damp-proofing, drainage membrane and weeping tile.</li>
                    <li>Professionally engineered roof trusses.</li>
                    <li>Steel beam construction where applicable.</li>
                    <li>Garage floors to be poured concrete.</li>
                    <li>All drywall secured with screws.</li>
                </ul>
                
                <h3>KITCHEN / BATH / LAUNDRY</h3>
                <ul>
                    <li>High quality, custom designed cabinetry.</li>
                    <li>Double stainless steel sink with single lever faucet in kitchen.</li>
                    <li>Durable, condensation-free and noise-free plastic PEX plumbing pipes and fittings throughout.</li>
                    <li>Pressured balanced temperature control valves in all ensuites and main bath shower enclosure.</li>
                    <li>Select Moen fixtures for all faucets.</li>
                    <li>Second ensuite in the front bedroom with walk-in glass shower.</li>
                    <li>Stand alone soaker tub and walk-in glass shower in master ensuite.</li>
                    <li>Main floor laundry with laundry shoot from upper loft area.</li>
                    <li>Comfortable heated floor in the master ensuite.</li>
                    <li>Towel warmer in master ensuite.</li>
                    <li>Ceramic tile kitchen backsplash.</li>
                </ul>
                
                <h3>HEATING / INSULATION / AIR CONDITIONING</h3>
                <ul>
                    <li>Insulation to meet or exceed Ontario Building Code Standards (R60 blown insulation in attic, R22 Batt insulation in exterior walls).</li>
                    <li>R20 basement insulation to within 8 inches of basement floor.</li>
                    <li>Heated floor throughout basement area.</li>
                    <li>High efficiency forced air single stage gas furnace with HRV system.</li>
                    <li>Exhaust fan in powder room and all bathrooms.</li>
                </ul>
                
                <h3>ELECTRICAL / MEDIA / SECURITY</h3>
                <ul>
                    <li>200 amp electrical service with copper wiring and circuit breaker in accordance with Electrical Safety Authority.</li>
                    <li>Electrical outlets in all bathrooms and powder room include ground fault interrupter (GFCI).</li>
                    <li>White rocker light switches throughout.</li>
                    <li>Direct wired electronic Smoke detector and Carbon Monoxide Detectors installed on each floor and bedrooms, as per Electrical Safety Authority.</li>
                    <li>Pre-wired for cable television in master bedroom and great room.</li>
                    <li>CAT6 network wiring in master, kitchen and great room.</li>
                    <li>Over seventy (70) pot lights through the main floor and loft area.</li>
                </ul>

                <div class="disclaimer">Price includes utility connection fees, boulevard tree planting, all development fees and education levies.</div>
                </div>
            </Modal.Body>
            <Modal.Footer closeButton><button className="close" onClick={handleClose}><span aria-hidden="true">×</span><span class="sr-only">Close</span></button></Modal.Footer>
        </Modal>
    </>
    )
}

export default FeaturesModal;

