import * as React from "react";

function SvgActivity(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="1em"
      height="1em"
      viewBox="0 0 24 24"
      {...props}
    >
      <path d="M22 11h-4c-.439 0-.812.283-.949.684L15 17.838 9.949 2.684a1.001 1.001 0 00-1.898 0L5.279 11H2a1 1 0 000 2h4c.423-.003.81-.267.949-.684L9 6.162l5.051 15.154a.999.999 0 001.897 0L18.721 13H22a1 1 0 000-2z" />
    </svg>
  );
}

export default SvgActivity;

