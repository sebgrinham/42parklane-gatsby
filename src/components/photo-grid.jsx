import React from "react";
import { StaticImage } from "gatsby-plugin-image";

export default class PhotoGrid extends React.Component {

        render() {
            return(
                <>
                <div className="photoGridTitle">~ August 24, 2021 ~</div>
                <div className="photoGrid">
                    <StaticImage src="../images/construction/2021-08-24/IMG_1553.jpg" alt="42 Park Lane – August 24, 2021 – Front, facing Park Lane"/>
                    <StaticImage src="../images/construction/2021-08-24/IMG_1554.jpg" alt="42 Park Lane – August 24, 2021 – Front entry"/>
                    <StaticImage src="../images/construction/2021-08-24/IMG_1551.jpg" alt="42 Park Lane – August 24, 2021 – Front entry"/>
                    <StaticImage src="../images/construction/2021-08-24/IMG_1559.jpg" alt="42 Park Lane – August 24, 2021 – Exterior corner detail"/>
                    
                    <StaticImage src="../images/construction/2021-08-24/IMG_1569.jpg" alt="42 Park Lane – August 24, 2021 – Ensuite porch"/>
                    <StaticImage src="../images/construction/2021-08-24/IMG_1566.jpg" alt="42 Park Lane – August 24, 2021 – Portico"/>
                    <StaticImage src="../images/construction/2021-08-24/IMG_1579.jpg" alt="42 Park Lane – August 24, 2021 – Stairs to second floor"/>
                    <StaticImage src="../images/construction/2021-08-24/IMG_1580.jpg" alt="42 Park Lane – August 24, 2021 – Entry, Principle bedroom"/>
                    
                    <StaticImage src="../images/construction/2021-08-24/IMG_1583.jpg" alt="42 Park Lane – August 24, 2021 – Principle bedroom"/>
                    <StaticImage src="../images/construction/2021-08-24/IMG_1601.jpg" alt="42 Park Lane – August 24, 2021 – Front entry"/>
                    <StaticImage src="../images/construction/2021-08-24/IMG_1608.jpg" alt="42 Park Lane – August 24, 2021 – Clerestory"/>
                    <StaticImage src="../images/construction/2021-08-24/IMG_1611.jpg" alt="42 Park Lane – August 24, 2021 – Second floor hallway"/>

                    <StaticImage src="../images/construction/2021-08-24/IMG_1616.jpg" alt="42 Park Lane – August 24, 2021 – Upstairs bedroom"/>
                    <StaticImage src="../images/construction/2021-08-24/IMG_1621.jpg" alt="42 Park Lane – August 24, 2021 – Upstairs bedroom"/>
                    <StaticImage src="../images/construction/2021-08-24/IMG_1595.jpg" alt="42 Park Lane – August 24, 2021 – Furnace"/>
                    <StaticImage src="../images/construction/2021-08-24/IMG_1599.jpg" alt="42 Park Lane – August 24, 2021 – Basement"/>
                </div>
                <div className="photoGridTitle">~ June 11, 2021 ~</div>
                <div className="photoGrid">
                    <StaticImage src="../images/construction/2021-06-11/IMG_1179.jpg" alt="42 Park Lane – June 11, 2021 – Window, exterior"/>
                    <StaticImage src="../images/construction/2021-06-11/IMG_1181.jpg" alt="42 Park Lane – June 11, 2021 – Living & dining area"/>
                    <StaticImage src="../images/construction/2021-06-11/IMG_1184.jpg" alt="42 Park Lane – June 11, 2021 – Stairs to second floor"/>
                    <StaticImage src="../images/construction/2021-06-11/IMG_1186.jpg" alt="42 Park Lane – June 11, 2021 – Principle bedroom"/>
                </div>
                <div className="photoGridTitle">~ May 7, 2021 ~</div>
                <div className="photoGrid">
                    <StaticImage src="../images/construction/2021-05-07/IMG_1042.jpeg" alt="42 Park Lane – May 7, 2021 – Front of house"/>    
                    <StaticImage src="../images/construction/2021-05-07/IMG_1015.jpeg" alt="42 Park Lane – May 7, 2021 – Garage, front of house"/>
                    <StaticImage src="../images/construction/2021-05-07/IMG_1012.jpeg" alt="42 Park Lane – May 7, 2021 – Garage, rear of house"/>
                    <StaticImage src="../images/construction/2021-05-07/IMG_1011.jpeg" alt="42 Park Lane – May 7, 2021 – Rear of house"/>

                    <StaticImage src="../images/construction/2021-05-07/IMG_1019.jpeg" alt="42 Park Lane – May 7, 2021 – Stone, brick detail"/>
                    <StaticImage src="../images/construction/2021-05-07/IMG_0998.jpeg" alt="42 Park Lane – May 7, 2021 – Front Entry"/>
                    <StaticImage src="../images/construction/2021-05-07/IMG_0997.jpeg" alt="42 Park Lane – May 7, 2021 – Principle bedroom"/>
                    <StaticImage src="../images/construction/2021-05-07/IMG_0993.jpeg" alt="42 Park Lane – May 7, 2021 – Window"/>
                    
                    <StaticImage src="../images/construction/2021-05-07/IMG_1024.jpeg" alt="42 Park Lane – May 7, 2021 – Window, bedroom 1"/>
                    <StaticImage src="../images/construction/2021-05-07/IMG_1029.jpeg" alt="42 Park Lane – May 7, 2021 – Window, bedroom 2"/>
                    <StaticImage src="../images/construction/2021-05-07/IMG_1034.jpeg" alt="42 Park Lane – May 7, 2021 – Principle bedroom, from above"/>
                    <StaticImage src="../images/construction/2021-05-07/IMG_1037.jpeg" alt="42 Park Lane – May 7, 2021 – Window, principle ensuite above future tub"/> 
                </div>
                <div className="photoGridTitle">~ March 19, 2021 ~</div>
                <div className="photoGrid">
                    <StaticImage src="../images/construction/2021-03-19/IMG_0690.jpeg" alt="42 Park Lane – March 19, 2021 – Front of house"/>
                    <StaticImage src="../images/construction/2021-03-19/IMG_0771.jpeg" alt="42 Park Lane – March 19, 2021 – Garage"/>
                    <StaticImage src="../images/construction/2021-03-19/IMG_0773.jpeg" alt="42 Park Lane – March 19, 2021 – Rear of house"/>
                    <StaticImage src="../images/construction/2021-03-19/IMG_0745.jpeg" alt="42 Park Lane – March 19, 2021 – Basement radiant heat tubing"/>
                    
                    <StaticImage src="../images/construction/2021-03-19/IMG_0712.jpeg" alt="42 Park Lane – March 19, 2021 – Principal ensuite corner window"/>
                    <StaticImage src="../images/construction/2021-03-19/IMG_0716.jpeg" alt="42 Park Lane – March 19, 2021 – Cantilevered stair, from below"/>
                    <StaticImage src="../images/construction/2021-03-19/IMG_0721.jpeg" alt="42 Park Lane – March 19, 2021 – Living room"/>
                    <StaticImage src="../images/construction/2021-03-19/IMG_0723.jpeg" alt="42 Park Lane – March 19, 2021 – Structural detail, main open space"/>
                </div>
                </>
            )
        }

}