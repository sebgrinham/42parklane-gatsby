import React from "react";
import Carousel from "react-bootstrap/Carousel";

import slide1 from "../images/lifestyle/almani-aXtYkEMJih4-unsplash.jpg";
import slide2 from "../images/lifestyle/gemma-evans-LTEo69JUv7o-unsplash.jpg";
import slide3 from "../images/lifestyle/kristel-hayes--BcnpZHZJx4-unsplash.jpg";
import slide4 from "../images/lifestyle/sandra-grunewald-3kK26U_Rw0s-unsplash.jpg";
import slide5 from "../images/lifestyle/terra-slaybaugh-7bk8q4AyJm8-unsplash.jpg";

export default function LifestyleSlideCarousel() {
    return (
        <Carousel style={{ background: "#EEE"}}>
            <Carousel.Item>
                <img
                className="d-block w-100"
                src={slide1}
                alt="Motorcyle cruising"
                />
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="d-block w-100"
                src={slide2}
                alt="Cyling tours"
                />
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="d-block w-100"
                src={slide3}
                alt="Sailing on the lake"
                />
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="d-block w-100"
                src={slide4}
                alt="Freshly picked strawberries"
                />
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="d-block w-100"
                src={slide5}
                alt="Ambrosia apples at harvest"
                />
            </Carousel.Item>
        </Carousel>
    )
}