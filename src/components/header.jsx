import React from "react";
import Image from "react-bootstrap/Image";
import SvgLoengroupLogo from "./svg/logo";

import headerImage from "../images/header-drawing.jpg";

export default function Header() {
    return (
        <header>
            <div className="logo-block">
                <SvgLoengroupLogo width='150px' height='150px'/>
                <div className="headings">
                    <h1>42 Park Lane</h1>
                    <h2>Bond Head, Newcastle</h2>
                    <h3 style={{ alignSelf: "flex-end"}}>Custom Home For Sale by Builder</h3>
                </div>
            </div> 
            <Image src={headerImage} fluid alt="42 Park Lane – Artist Concept" width="1600" height="639"/>
            <nav>
                <ul>
                    <li><a href="#location">Location</a></li>
                    <li><a href="#features">Features</a></li>
                    <li><a href="#progress">Progress</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </nav>
        </header>
    )
}