import React from "react";
import addToMailchimp from 'gatsby-plugin-mailchimp';

export default class SubscribeForm extends React.Component {

    state = {
        email: '',
        message: ''
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        const result = await addToMailchimp(this.state.email);
        this.setState({ message: result.msg });
        console.log('result',result);
    }

    render() {
        return (
            <div id="subscribeForm">
                <div className="content">
                <h2 className="subscribe">Subscribe to Updates</h2>
                <form method="POST" onSubmit={this.handleSubmit}>
                    <div className="message" dangerouslySetInnerHTML={{ __html: this.state.message }} />
                    <div className="form-row">
                        <label>Email
                        <input 
                            type="email" 
                            name="email" 
                            id="email" 
                            value={this.state.email}
                            onChange={this.handleInputChange}
                        />
                        </label>
                    </div>
                    <button type="submit">Get Updates</button>
                </form>
                </div>
            </div>
        );
    }
}