import React from "react";
import Carousel from "react-bootstrap/Carousel";

import slide1 from "../images/floorplans/42Parklane-floorplan-basement-ready.jpg";
import slide2 from "../images/floorplans/42Parklane-floorplan-main-ready.jpg";
import slide3 from "../images/floorplans/42Parklane-floorplan-second-floor-ready.jpg";

import downloadPDF from '../PDF/42Parklane-floorplan.pdf';


export default function FloorplanSlideCarousel() {
    return (
        <>
        <Carousel className="floorplans">
            <Carousel.Item>
                <img
                className="d-block w-100"
                src={slide1}
                alt="Basement floorplan"
                />
                <Carousel.Caption>
                <div className="caption">Basement</div>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="d-block w-100"
                src={slide2}
                alt="Main Floor floorplan"
                />

                <Carousel.Caption>
                <div className="caption">Main Floor</div>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="d-block w-100"
                src={slide3}
                alt="Second Floor floorplan"
                />

                <Carousel.Caption>
                <div className="caption">Second Floor</div>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
        <div className="downloadPDFlink">
            <a href={downloadPDF}>Download Floorplans in PDF</a>{` `}
        </div>
        </>
    )
}