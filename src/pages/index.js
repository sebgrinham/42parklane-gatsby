import React from 'react';
import { Helmet } from 'react-helmet';
import Image from "react-bootstrap/Image";
// import { StaticImage } from "gatsby-plugin-image"

// import rendering components
import Header from '../components/header.jsx';
import FloorplanSlideCarousel from '../components/carousel-floorplans.jsx';
import LifestyleSlideCarousel from '../components/carousel-lifestyle.jsx';
import SocialLinks from '../components/social-links.jsx';
import SubscribeForm from '../components/subscribe-form.jsx';
import PhotoGrid from '../components/photo-grid.jsx';
import FeaturesModal from '../components/features-modal.jsx';

// import inline images
import lotLines from '../images/42-Park-Lane---lot-lines.jpg';
import locationMap from '../images/apple-maps-screenshot.png';

// import SVG objects
import SvgHome from "../components/svg/home";
import SvgMap from "../components/svg/map";
import SvgClock from "../components/svg/clock";
import SvgCheckCircle from "../components/svg/check-circle";
import SvgActivity from "../components/svg/activity";
import SvgAnchor from "../components/svg/anchor";
import SvgAtSign from "../components/svg/at-sign";
import SvgMail from "../components/svg/mail";
import SvgFacebook from '../components/svg/facebook_logo';
import SvgTwitter from '../components/svg/twitter_logo';

// import stylesheets
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/index.css';


function Index() {
  return (
    <main>
      <Helmet htmlAttributes={{ lang: 'en' }}>
        <title>42 Park Lane: Custom Home for Sale in Bond Head, Newcastle.</title>
        <meta name="description" content="Helmet application" />
      </Helmet>
      
      <Header />
      
      <section id="about">
        <h2 className="section_title"><SvgHome /> About</h2>
        <h1>A rare opportunity in a uniquely beautiful setting</h1>
        <p>42 Park Lane is being developed by <a href="https://theloengroup.ca">The Loën Group</a>, a local builder with extensive experience building custom high-end homes that showcase their commitment to quality workmanship & materials.</p>
        <p>This will be the last home built in a quiet enclave of upscale executive homes located just a short walk from the shores of Lake Ontario.</p>
        <p>Ground broke on the project in late 2020, and construction is expected to be completed in early 2022.</p>
        <p>Asking price: $3,290,000.  Broker protected.</p>
        <p>Please direct all sales inquiries to <a href="mailto:info@42parklane.ca">info@42parklane.com</a></p>
      </section>
      
      <FloorplanSlideCarousel />

      <section id="subscribe_social">
          <p><a href="#subscribeForm">Subscribe for updates</a> and we'll send you an email after the next major construction milestone is met.</p>
          <p>You can also follow us on Twitter, Facebook and Instargram for updates and announcments.</p>
          <SocialLinks />
      </section>

      <Image src={lotLines} fluid  style={{ width: "100%" }} alt="42 Park Lane – Lot lines; aerial photo of Park Lane, Bond Head" width="1600" height="870"/>

      <section id="location">
        <h2 className="section_title"><SvgMap />Location</h2>
        <p>42 Park Lane is located in the lakeside community of Historical Bond Head, Newcastle.</p>
        <p>The area is known for it's apple orchards, fruit picking, and beautiful contry roads perfect for cycling trips or motorcycle cruising.</p>
        <p>Nearby Newcastle is a five minute drive to:</p>
            <ul>
                <li>Recreation center with olympic-size indoor pool</li>
                <li>Public library</li>
                <li>GO bus stop</li>
                <li>No Frills &amp; Foodland</li>
                <li>Butcher</li>
                <li>Restaurants</li>
                <li>LCBO</li>
                <li>Home Hardware</li>
                <li>Shoppers Drug Mart</li>
                <li>Tim Hortons</li>
                <li>Dry cleaners</li>
                <li>CIBC branch</li>
            </ul>
        <hr style={{ width: "50%", margin: "1rem auto", minWidth: "300px", marginBottom: "3rem" }} / >
        
        <SvgClock width="40px" height="40px" style={{ width: "100%", textAlign: "center", marginBottom: "2rem"  }} />
        
        <ul className="fancyList sansBullets">
            <li>1 hour to downtown Toronto</li>
            <li>1 hour to YYZ international airport</li>
            <li>Easy access to 407</li>
            <li>20 minutes to Brimacombe Ski Resort</li>
            <li>10 minutes to Newcastle Golf Club</li>
            <li>5 minutes to Port of Newcastle Marina</li>
        </ul>
      </section>  

      <Image src={locationMap} fluid style={{ width: "100%" }} alt="42 Park Lane – Area map, showing location relative to Oshawa" width="1226" height="528"/>

      <section id="features">
        <h2 className="section_title"><SvgCheckCircle /> Features</h2>
          <ul>
              <li>1.34 acre lot</li>
              <li>3000+ sqft</li>
              <li>4 bed, 3.5 bath</li>
              <li>Attached extra-deep 3 car garage</li>
              <li>Muncipal water supply</li>
              <li>Contemporary wall & ceiling details</li>
              <li>Custom designed cantilivered stairs</li>
              <li>Hardwood & ceramic tile flooring</li>
              <li>Gas fireplace</li>
              <li>Three window clerestory</li>
              <li>Large open living spaces + high ceilings</li>
              <li>Coffered ceiling in principal bedroom</li>
              <li>Principal ensuite with heated floors</li>
              <li>Finished basement with heated floors</li>
              <li>New Home Tarion warranty included</li>
              <li style={{ listStyle: "none", marginTop: "3rem"}}><FeaturesModal /></li>
          </ul>
          
      </section>

      <LifestyleSlideCarousel />

      <section id="progress">
          <h2 className="section_title"><SvgActivity /> Progress</h2>
          <PhotoGrid />
      </section>

      <SubscribeForm />

      <section id="local_links">
          <h2 className="section_title"><SvgAnchor /> Local Links</h2>
            <ul className="sansBullets" style={{ textAlign: "center"}}>
                <li><a href="https://villageofnewcastle.ca/">Village of Newcastle</a></li>
                <li><a href="https://www.clarington.net/">Municipality of Clarington</a></li>
                <li><a href="https://www.theweathernetwork.com/ca/weather/ontario/newcastle">Weather Network - Newcastle, ON</a></li>
                <li><a href="https://elexiconenergy.com/">Elexicon Energy(Power Utility)</a></li>
                <li><a href="https://www.newcastlegolf.ca/">Newcastle Golf and Country Club</a></li>
                <li><a href="https://brimacombe.ca/">Brimacombe Ski</a></li>
                <li><a href="https://www.grca.on.ca/">Ganaraska Conservation</a></li>
            </ul>
      </section>

      <div className="washedStones">
      </div>

      <section id="contact">
          <h2 className="section_title"><SvgAtSign /> Contact</h2>
            <ul className="sansBullets" style={{ textAlign: "center" }}>
                <li><a href="mailto:info@42parklane.ca"><SvgMail/>info@42parklane.ca</a></li>
                <li><a href="https://facebook.com/loengroup"><SvgFacebook/>facebook.com/loengroup</a></li>
                <li><a href="https://twitter.com/loengroup"><SvgTwitter/>@loengroup</a></li>
            </ul>
      </section>

      <footer>
        <p>&copy;2022 The Loën Group.  All rights reserved.</p>
      </footer>

    </main>
  );
}

export default Index;
